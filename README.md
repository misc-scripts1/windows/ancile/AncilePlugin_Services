# Ancile Services
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_Services

Ancile Services Allows the disabling of Windows services by adding simple configuration files to the data directory.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile